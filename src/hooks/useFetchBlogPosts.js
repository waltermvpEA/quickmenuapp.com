import {useCallback, useEffect, useState} from "react";
import dummyBlogPosts from "../logged_out/dummy_data/blogPosts";

export const fetchBlogs = () => dummyBlogPosts.map((blogPost) => {
    let title = blogPost.title;
    title = title.toLowerCase();
    /* Remove unwanted characters, only accept alphanumeric and space */
    title = title.replace(/[^A-Za-z0-9 ]/g, "");
    /* Replace multi spaces with a single space */
    title = title.replace(/\s{2,}/g, " ");
    /* Replace space with a '-' symbol */
    title = title.replace(/\s/g, "-");
    blogPost.url = `/blog/post/${title}`;
    blogPost.urlTitle = `${title}`;
    blogPost.params = `?id=${blogPost.id}`;
    return blogPost;
});

const useFetchBlogPosts = (initialBlog = []) => {
    const [blogPosts, setBlogPosts] = useState(initialBlog);

    const fetchBlogPosts = useCallback(() => {
        const blogPosts = fetchBlogs()
        setBlogPosts(blogPosts);
    }, [setBlogPosts]);
    useEffect(fetchBlogPosts, []);
    return {
        blogPosts,
        setBlogPosts
    }
}

export default useFetchBlogPosts
