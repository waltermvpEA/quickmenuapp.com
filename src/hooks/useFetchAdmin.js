import {useCallback, useEffect, useState} from "react";
import persons from "../logged_in/dummy_data/persons";
function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
export const useFetchRandomTransactions = () => {
    const [transactions, setTransactions] = useState([]);
    const fetchRandomTransactions = useCallback(() => {
        const transactions = [];
        const iterations = 32;
        const oneMonthSeconds = Math.round(60 * 60 * 24 * 30.5);
        const transactionTemplates = [
            {
                description: "Starter subscription",
                isSubscription: true,
                balanceChange: -1499,
            },
            {
                description: "Premium subscription",
                isSubscription: true,
                balanceChange: -2999,
            },
            {
                description: "Business subscription",
                isSubscription: true,
                balanceChange: -4999,
            },
            {
                description: "Tycoon subscription",
                isSubscription: true,
                balanceChange: -9999,
            },
            {
                description: "Added funds",
                isSubscription: false,
                balanceChange: 2000,
            },
            {
                description: "Added funds",
                isSubscription: false,
                balanceChange: 5000,
            },
        ];
        let curUnix = Math.round(
            new Date().getTime() / 1000 - iterations * oneMonthSeconds
        );
        for (let i = 0; i < iterations; i += 1) {
            const randomTransactionTemplate =
                transactionTemplates[
                    Math.floor(Math.random() * transactionTemplates.length)
                    ];
            const transaction = {
                id: i,
                description: randomTransactionTemplate.description,
                balanceChange: randomTransactionTemplate.balanceChange,
                paidUntil: curUnix + oneMonthSeconds,
                timestamp: curUnix,
            };
            curUnix += oneMonthSeconds;
            transactions.push(transaction);
        }
        transactions.reverse();
        setTransactions(transactions);
    }, [setTransactions]);
    useEffect(()=>{
        fetchRandomTransactions()
    },[fetchRandomTransactions])
    return {
        transactions,
        setTransactions
    }
}

export const useFetchRandomMessages = () => {

    const [messages, setMessages] = useState([]);
    const fetchRandomMessages = useCallback(() => {
        shuffle(persons);
        const messages = [];
        const iterations = persons.length;
        const oneDaySeconds = 60 * 60 * 24;
        let curUnix = Math.round(
            new Date().getTime() / 1000 - iterations * oneDaySeconds
        );
        for (let i = 0; i < iterations; i += 1) {
            const person = persons[i];
            const message = {
                id: i,
                src: person.src,
                date: curUnix,
                text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr sed.",
            };
            curUnix += oneDaySeconds;
            messages.push(message);
        }
        messages.reverse();
        setMessages(messages);
    }, [setMessages]);
    useEffect(()=>{
        fetchRandomMessages()
    },[fetchRandomMessages])
    return {
        messages,
        setMessages
    }
}

export const useFetchRandomPosts = () => {

    const [posts, setPosts] = useState([]);
    const fetchRandomPosts = useCallback(() => {
        shuffle(persons);
        const posts = [];
        const iterations = persons.length;
        const oneDaySeconds = 60 * 60 * 24;
        let curUnix = Math.round(
            new Date().getTime() / 1000 - iterations * oneDaySeconds
        );
        for (let i = 0; i < iterations; i += 1) {
            const person = persons[i];
            const post = {
                id: i,
                src: person.src,
                timestamp: curUnix,
                name: person.name,
            };
            curUnix += oneDaySeconds;
            posts.push(post);
        }
        posts.reverse();
        setPosts(posts);
    }, [setPosts]);
    useEffect(()=>{
        fetchRandomPosts()
    },[fetchRandomPosts])
    return {
        setPosts,
        posts
    }
}

export const useFetchRandomTargets = () => {
    const [targets, setTargets] = useState([]);
    const fetchRandomTargets = useCallback(() => {
        const targets = [];
        for (let i = 0; i < 35; i += 1) {
            const randomPerson = persons[Math.floor(Math.random() * persons.length)];
            const target = {
                id: i,
                number1: Math.floor(Math.random() * 251),
                number2: Math.floor(Math.random() * 251),
                number3: Math.floor(Math.random() * 251),
                number4: Math.floor(Math.random() * 251),
                name: randomPerson.name,
                profilePicUrl: randomPerson.src,
                isActivated: Math.round(Math.random()) ? true : false,
            };
            targets.push(target);
        }
        setTargets(targets);
    }, [setTargets]);
    useEffect(()=> {
        fetchRandomTargets()
    },[fetchRandomTargets])
    return {targets, setTargets}
}

export const useToggleAccountActivation = () => {
    const [isAccountActivated, setIsAccountActivated] = useState(false);
    const [pushMessageToSnackbar, setPushMessageToSnackbar] = useState(null);
    const toggleAccountActivation = useCallback(() => {
        if (pushMessageToSnackbar) {
            if (isAccountActivated) {
                pushMessageToSnackbar({
                    text: "Your account is now deactivated.",
                });
            } else {
                pushMessageToSnackbar({
                    text: "Your account is now activated.",
                });
            }
        }
        setIsAccountActivated(!isAccountActivated);
    }, [pushMessageToSnackbar, isAccountActivated, setIsAccountActivated]);

    return {isAccountActivated, setIsAccountActivated, pushMessageToSnackbar, setPushMessageToSnackbar, toggleAccountActivation}
}

export const useFetchRandomStatistics = () => {
    const [statistics, setStatistics] = useState({ views: [], profit: [] });
    const fetchRandomStatistics = useCallback(() => {
        const statistics = { profit: [], views: [] };
        const iterations = 300;
        const oneYearSeconds = 60 * 60 * 24 * 365;
        let curProfit = Math.round(3000 + Math.random() * 1000);
        let curViews = Math.round(3000 + Math.random() * 1000);
        let curUnix = Math.round(new Date().getTime() / 1000) - oneYearSeconds;
        for (let i = 0; i < iterations; i += 1) {
            curUnix += Math.round(oneYearSeconds / iterations);
            curProfit += Math.round((Math.random() * 2 - 1) * 10);
            curViews += Math.round((Math.random() * 2 - 1) * 10);
            statistics.profit.push({
                value: curProfit,
                timestamp: curUnix,
            });
            statistics.views.push({
                value: curViews,
                timestamp: curUnix,
            });
        }
        setStatistics(statistics);
    }, [setStatistics]);
    useEffect(()=>{
        fetchRandomStatistics()
    },[fetchRandomStatistics])

    return {
        statistics,
        setStatistics
    }
}
