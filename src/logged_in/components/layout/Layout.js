import React, { memo, useCallback, useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core";
import NavBar from "../navigation/NavBar";
import ConsecutiveSnackbarMessages from "../../../shared/components/ConsecutiveSnackbarMessages";
import smoothScrollTop from "../../../shared/functions/smoothScrollTop";
import persons from "../../dummy_data/persons";
import LazyLoadAddBalanceDialog from "../subscription/LazyLoadAddBalanceDialog";
import {useFetchRandomMessages} from "../../../hooks/useFetchAdmin";

const styles = (theme) => ({
    main: {
        marginLeft: theme.spacing(9),
        transition: theme.transitions.create(["width", "margin"], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        [theme.breakpoints.down("xs")]: {
            marginLeft: 0,
        },
    },
    wrapper: {
        margin: theme.spacing(1),
        width: "auto",
        [theme.breakpoints.up("xs")]: {
            width: "95%",
            marginLeft: "auto",
            marginRight: "auto",
            marginTop: theme.spacing(4),
            marginBottom: theme.spacing(4),
        },
        [theme.breakpoints.up("sm")]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            width: "90%",
            marginLeft: "auto",
            marginRight: "auto",
        },
        [theme.breakpoints.up("md")]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            width: "82.5%",
            marginLeft: "auto",
            marginRight: "auto",
        },
        [theme.breakpoints.up("lg")]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            width: "70%",
            marginLeft: "auto",
            marginRight: "auto",
        },
    }
});

function Main(props) {
    const { classes, children, isAddBalanceDialogOpen, setIsAddBalanceDialogOpen, pushMessageToSnackbar, setPushMessageToSnackbar  } = props;
    const {messages} = useFetchRandomMessages()

    const openAddBalanceDialog = useCallback(() => {
        setIsAddBalanceDialogOpen(true);
    }, [setIsAddBalanceDialogOpen]);

    const closeAddBalanceDialog = useCallback(() => {
        setIsAddBalanceDialogOpen(false);
    }, [setIsAddBalanceDialogOpen]);

    const onPaymentSuccess = useCallback(() => {
        pushMessageToSnackbar({
            text: "Your balance has been updated.",
        });
        setIsAddBalanceDialogOpen(false);
    }, [pushMessageToSnackbar, setIsAddBalanceDialogOpen]);


    const getPushMessageFromChild = useCallback(
        (pushMessage) => {
            setPushMessageToSnackbar(() => pushMessage);
        },
        [setPushMessageToSnackbar]
    );
    return (
        <Fragment>
            <LazyLoadAddBalanceDialog
                open={isAddBalanceDialogOpen}
                onClose={closeAddBalanceDialog}
                onSuccess={onPaymentSuccess}
            />
            <NavBar
                messages={messages}
                openAddBalanceDialog={openAddBalanceDialog}
            />
            <ConsecutiveSnackbarMessages
                getPushMessageFromChild={getPushMessageFromChild}
            />
            <main className={classNames(classes.main)}>

                <div className={classes.wrapper}>
                {children}
                </div>
            </main>

        </Fragment>
    );
}

Main.propTypes = {
    classes: PropTypes.object.isRequired,
    isAddBalanceDialogOpen: PropTypes.bool.isRequired,
    setIsAddBalanceDialogOpen: PropTypes.func.isRequired,
    pushMessageToSnackbar: PropTypes.func,
    setPushMessageToSnackbar: PropTypes.func
};

export default withStyles(styles, { withTheme: true })(memo(Main));
