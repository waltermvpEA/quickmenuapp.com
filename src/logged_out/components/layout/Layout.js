import React, { memo, useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import AOS from "aos/dist/aos";
import { withStyles } from "@material-ui/core";
import NavBar from "../navigation/NavBar";
import Footer from "../footer/Footer";
import "aos/dist/aos.css";
import CookieRulesDialog from "../cookies/CookieRulesDialog";
import dummyBlogPosts from "../../dummy_data/blogPosts";
import DialogSelector from "../register_login/DialogSelector";
import smoothScrollTop from "../../../shared/functions/smoothScrollTop";
import dynamic from "next/dynamic";

if(typeof document !== "undefined"){
    AOS.init({ once: true });
}

const styles = (theme) => ({
    wrapper: {
        backgroundColor: theme.palette.common.white,
        overflowX: "hidden",
    },
});

const DynamicCookieConsent = dynamic(() => import("../cookies/CookieConsent"), {
    ssr: false
})

function Layout(props) {
    const { classes, children} = props;
    const [isMobileDrawerOpen, setIsMobileDrawerOpen] = useState(false);
    const [dialogOpen, setDialogOpen] = useState(null);
    const [isCookieRulesDialogOpen, setIsCookieRulesDialogOpen] = useState(false);

    const openLoginDialog = useCallback(() => {
        setDialogOpen("login");
        setIsMobileDrawerOpen(false);
    }, [setDialogOpen, setIsMobileDrawerOpen]);

    const closeDialog = useCallback(() => {
        setDialogOpen(null);
    }, [setDialogOpen]);

    const openRegisterDialog = useCallback(() => {
        setDialogOpen("register");
        setIsMobileDrawerOpen(false);
    }, [setDialogOpen, setIsMobileDrawerOpen]);

    const openTermsDialog = useCallback(() => {
        setDialogOpen("termsOfService");
    }, [setDialogOpen]);

    const handleMobileDrawerOpen = useCallback(() => {
        setIsMobileDrawerOpen(true);
    }, [setIsMobileDrawerOpen]);

    const handleMobileDrawerClose = useCallback(() => {
        setIsMobileDrawerOpen(false);
    }, [setIsMobileDrawerOpen]);

    const openChangePasswordDialog = useCallback(() => {
        setDialogOpen("changePassword");
    }, [setDialogOpen]);

    const handleCookieRulesDialogOpen = useCallback(() => {
        setIsCookieRulesDialogOpen(true);
    }, [setIsCookieRulesDialogOpen]);

    const handleCookieRulesDialogClose = useCallback(() => {
        setIsCookieRulesDialogOpen(false);
    }, [setIsCookieRulesDialogOpen]);

    return (
        <div className={classes.wrapper}>
            {!isCookieRulesDialogOpen && (
                <DynamicCookieConsent
                    handleCookieRulesDialogOpen={handleCookieRulesDialogOpen}
                />
            )}
            <DialogSelector
                openLoginDialog={openLoginDialog}
                dialogOpen={dialogOpen}
                onClose={closeDialog}
                openTermsDialog={openTermsDialog}
                openRegisterDialog={openRegisterDialog}
                openChangePasswordDialog={openChangePasswordDialog}
            />
            <CookieRulesDialog
                open={isCookieRulesDialogOpen}
                onClose={handleCookieRulesDialogClose}
            />
            <NavBar
                openLoginDialog={openLoginDialog}
                openRegisterDialog={openRegisterDialog}
                mobileDrawerOpen={isMobileDrawerOpen}
                handleMobileDrawerOpen={handleMobileDrawerOpen}
                handleMobileDrawerClose={handleMobileDrawerClose}
            />
            {/*blogPosts={blogPosts}*/}
            {/*selectHome={selectHome}*/}
            {/*selectBlog={selectBlog}*/}
            {children}
            <Footer />
        </div>
    );
}

Layout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(Layout);
