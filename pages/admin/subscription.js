import React, {useCallback, useState} from 'react';
import Layout from "../../src/logged_in/components/layout/Layout";
import Subscription from "../../src/logged_in/components/subscription/Subscription";
import {useFetchRandomTransactions} from "../../src/hooks/useFetchAdmin";

function SubscriptionPage() {
    const { transactions } = useFetchRandomTransactions()
    const [pushMessageToSnackbar, setPushMessageToSnackbar] = useState(null);
    const [isAddBalanceDialogOpen, setIsAddBalanceDialogOpen] = useState(false);

    const openAddBalanceDialog = useCallback(() => {
        setIsAddBalanceDialogOpen(true);
    }, [setIsAddBalanceDialogOpen]);


    return <Layout isAddBalanceDialogOpen={isAddBalanceDialogOpen} setIsAddBalanceDialogOpen={setIsAddBalanceDialogOpen} pushMessageToSnackbar={pushMessageToSnackbar} setPushMessageToSnackbar={setPushMessageToSnackbar}>
        <Subscription
            transactions={transactions}
            pushMessageToSnackbar={pushMessageToSnackbar}
            openAddBalanceDialog={openAddBalanceDialog}
        />

    </Layout>

}


export default SubscriptionPage
