import React, {useEffect, useState, useCallback} from 'react'
import Layout from "../../src/logged_in/components/layout/Layout";
import Posts from "../../src/logged_in/components/posts/Posts";
import {useFetchRandomPosts} from "../../src/hooks/useFetchAdmin";

const PostPage = () => {
    const [isAddBalanceDialogOpen, setIsAddBalanceDialogOpen] = useState(false);
    const [Dropzone, setDropzone] = useState(null);
    const [EmojiTextArea, setEmojiTextArea] = useState(null);
    const { posts, setPosts } = useFetchRandomPosts()
    const [hasFetchedEmojiTextArea, setHasFetchedEmojiTextArea] = useState(false);
    const [hasFetchedDropzone, setHasFetchedDropzone] = useState(false);
    const [ImageCropper, setImageCropper] = useState(null);
    const [hasFetchedImageCropper, setHasFetchedImageCropper] = useState(false);
    const [DateTimePicker, setDateTimePicker] = useState(null);
    const [hasFetchedDateTimePicker, setHasFetchedDateTimePicker] = useState(
        false
    );
    const [pushMessageToSnackbar, setPushMessageToSnackbar] = useState(null);
    const selectPosts = useCallback(() => {
        if (!hasFetchedEmojiTextArea) {
            setHasFetchedEmojiTextArea(true);
            import("../../src/shared/components/EmojiTextArea").then((Component) => {
                setEmojiTextArea(Component.default);
            });
        }
        if (!hasFetchedImageCropper) {
            setHasFetchedImageCropper(true);
            import("../../src/shared/components/ImageCropper").then((Component) => {
                setImageCropper(Component.default);
            });
        }
        if (!hasFetchedDropzone) {
            setHasFetchedDropzone(true);
            import("../../src/shared/components/Dropzone").then((Component) => {
                setDropzone(Component.default);
            });
        }
        if (!hasFetchedDateTimePicker) {
            setHasFetchedDateTimePicker(true);
            import("../../src/shared/components/DateTimePicker").then((Component) => {
                setDateTimePicker(Component.default);
            });
        }
    }, [
        setEmojiTextArea,
        setImageCropper,
        setDropzone,
        setDateTimePicker,
        hasFetchedEmojiTextArea,
        setHasFetchedEmojiTextArea,
        hasFetchedImageCropper,
        setHasFetchedImageCropper,
        hasFetchedDropzone,
        setHasFetchedDropzone,
        hasFetchedDateTimePicker,
        setHasFetchedDateTimePicker,
    ]);
        return <Layout isAddBalanceDialogOpen={isAddBalanceDialogOpen} setIsAddBalanceDialogOpen={setIsAddBalanceDialogOpen} pushMessageToSnackbar={pushMessageToSnackbar} setPushMessageToSnackbar={setPushMessageToSnackbar}>
        <Posts EmojiTextArea={EmojiTextArea}
               ImageCropper={ImageCropper}
               Dropzone={Dropzone}
               DateTimePicker={DateTimePicker}
               pushMessageToSnackbar={pushMessageToSnackbar}
               posts={posts}
               setPosts={setPosts}
               selectPosts={selectPosts} />
    </Layout>

}

export default PostPage
