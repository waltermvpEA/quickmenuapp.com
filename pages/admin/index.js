import React, {useState, useCallback, useEffect} from 'react';
import Layout from "../../src/logged_in/components/layout/Layout";
import Dashboard from "../../src/logged_in/components/dashboard/Dashboard";
import {useFetchRandomStatistics, useFetchRandomTargets} from "../../src/hooks/useFetchAdmin";

const Admin = () => {
    const {targets, setTargets} = useFetchRandomTargets()
    const {statistics} = useFetchRandomStatistics()
    const [CardChart, setCardChart] = useState(null);
    const [hasFetchedCardChart, setHasFetchedCardChart] = useState(false);
    const [pushMessageToSnackbar, setPushMessageToSnackbar] = useState(null);
    const [isAddBalanceDialogOpen, setIsAddBalanceDialogOpen] = useState(false);
    const [isAccountActivated, setIsAccountActivated] = useState(false);
    const toggleAccountActivation = useCallback(() => {
        if (pushMessageToSnackbar) {
            if (isAccountActivated) {
                pushMessageToSnackbar({
                    text: "Your account is now deactivated.",
                });
            } else {
                pushMessageToSnackbar({
                    text: "Your account is now activated.",
                });
            }
        }
        setIsAccountActivated(!isAccountActivated);
    }, [pushMessageToSnackbar, isAccountActivated, setIsAccountActivated]);

    const selectDashboard = useCallback(() => {
        if (!hasFetchedCardChart) {
            setHasFetchedCardChart(true);
            import("../../src/shared/components/CardChart").then((Component) => {
                setCardChart(Component.default);
            });
        }
    }, [
        setCardChart,
        hasFetchedCardChart,
        setHasFetchedCardChart,
    ]);

    return <Layout isAddBalanceDialogOpen={isAddBalanceDialogOpen} setIsAddBalanceDialogOpen={setIsAddBalanceDialogOpen} pushMessageToSnackbar={pushMessageToSnackbar} setPushMessageToSnackbar={setPushMessageToSnackbar}>
        <Dashboard toggleAccountActivation={toggleAccountActivation} statistics={statistics} targets={targets} setTargets={setTargets} selectDashboard={selectDashboard} CardChart={CardChart} />
    </Layout>
};

export default Admin;
