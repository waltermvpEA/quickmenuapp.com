import React  from "react";
import theme from "../src/theme";
import {CssBaseline, MuiThemeProvider} from "@material-ui/core";
import GlobalStyles from "../src/GlobalStyles";
import dynamic from "next/dynamic";

const DynamicPace = dynamic(()=>import("../src/shared/components/Pace"), {
  ssr: false
})

function MyApp({ Component, pageProps }) {
  return <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <GlobalStyles />
    <DynamicPace color={theme.palette.primary.light} />
    <Component {...pageProps} />
  </MuiThemeProvider>
}

export default MyApp
