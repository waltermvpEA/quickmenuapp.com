import React, { useEffect } from "react";
import PropTypes from "prop-types";
import BlogPost from "../../../src/logged_out/components/blog/BlogPost";
import {useRouter} from "next/router";
import {fetchBlogs} from "../../../src/hooks/useFetchBlogPosts";
import Layout from "../../../src/logged_out/components/layout/Layout";

const Post = (props) => {

    const {title, src, date, content, otherArticles } = props;

    return <Layout>
        <BlogPost
            title={title}
            src={src}
            date={date}
            content={content}
            otherArticles={otherArticles}
        />
    </Layout>
}
export async function getServerSideProps(context) {

    const { pid } = context.params;
    const blogPosts = fetchBlogs()
    const post = blogPosts.find(blog => blog.urlTitle === pid)
    const otherArticles = blogPosts.filter(blog => blog.id !== post?.id)
    if(!post){
        return {
            props: {
                otherArticles
            }
        }
    }
    return {
        props: {
            title: post.title,
            src: post.src,
            date: post.date || new Date(),
            content: "",
            otherArticles: otherArticles,
        }, // will be passed to the page component as props
    }
}


export default Post
