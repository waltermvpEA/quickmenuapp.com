import React, {memo} from "react";
import PropTypes from "prop-types";
import AOS from "aos/dist/aos";
import { withStyles } from "@material-ui/core";
import "aos/dist/aos.css";
import Layout from "../src/logged_out/components/layout/Layout";
import HeadSection from "../src/logged_out/components/home/HeadSection";
import FeatureSection from "../src/logged_out/components/home/FeatureSection";
import PricingSection from "../src/logged_out/components/home/PricingSection";

if(typeof document !== "undefined"){
  AOS.init({ once: true });
}

const styles = (theme) => ({
  wrapper: {
    backgroundColor: theme.palette.common.white,
    overflowX: "hidden",
  },
});

function Main(props) {
  return (
        <Layout >
          <HeadSection />
          <FeatureSection />
          <PricingSection />
        </Layout>
  );
}

Main.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(memo(Main));
